import React, { useEffect, useState } from 'react';

const AttendConferenceForm = () => {
  const [conferences, setConferences] = useState([]);
  const [conference, setConference] = useState('');
  const [email, setEmail] = useState('');
  const [fullname, setFullname] = useState('');
  const [dataLoaded, setDataLoaded] = useState(false);
  const [signupSuccess, setSignupSuccess] = useState(false);

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
      setDataLoaded(true);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      conference,
      email,
      name: fullname,
    };
    const attendeeUrl = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
      setSignupSuccess(true);
      const newAttendee = await response.json();
      console.log(newAttendee);
      setFullname('');
      setEmail('');
      setConference('');
    }
  };

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img
            width="300"
            className="bg-white rounded shadow d-block mx-auto mb-4"
            src="/logo.svg"
          />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form
                className={`${signupSuccess ? 'd-none' : ''}`}
                id="create-attendee-form"
              >
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference you'd like to attend.
                </p>
                <div
                  className={`d-flex justify-content-center mb-3 ${
                    dataLoaded ? 'd-none' : ''
                  }`}
                  id="loading-conference-spinner"
                >
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select
                    onChange={(e) => setConference(e.target.value)}
                    value={conference}
                    name="conference"
                    id="conference"
                    className={`form-select ${dataLoaded ? '' : 'd-none'}`}
                    required
                  >
                    <option value="">Choose a conference</option>
                    {conferences.map((conference) => (
                      <option key={conference.href} value={conference.href}>
                        {conference.name}
                      </option>
                    ))}
                  </select>
                </div>
                <p className="mb-3">Now, tell us about yourself.</p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input
                        required
                        onChange={(e) => setFullname(e.target.value)}
                        value={fullname}
                        placeholder="Your full name"
                        type="text"
                        id="name"
                        name="name"
                        className="form-control"
                      />
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input
                        required
                        onChange={(e) => setEmail(e.target.value)}
                        value={email}
                        placeholder="Your email address"
                        type="email"
                        id="email"
                        name="email"
                        className="form-control"
                      />
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button
                  onClick={handleSubmit}
                  className="btn btn-lg btn-primary"
                >
                  I'm going!
                </button>
              </form>
              <div
                className={`alert alert-success mb-0 ${
                  signupSuccess ? '' : 'd-none'
                }`}
                id="success-message"
              >
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AttendConferenceForm;
