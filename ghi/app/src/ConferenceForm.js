import React, { useEffect, useState } from 'react';

const ConferenceForm = () => {
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maxPresentations, setMaxPresentations] = useState(0);
  const [maxAttendees, setMaxAttendees] = useState(0);
  const [location, setLocation] = useState({});
  const [locations, setLocations] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      name,
      starts,
      ends,
      description,
      max_presentations: maxPresentations,
      max_attendees: maxAttendees,
      location,
    };
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaxPresentations(0);
      setMaxAttendees(0);
      setLocation({});
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setName(e.target.value)}
                value={name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setStarts(e.target.value)}
                value={starts}
                placeholder="mm/dd/yyyy"
                required
                type="date"
                name="starts"
                id="starts"
                className="form-control"
              />
              <label htmlFor="starts">Start Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setEnds(e.target.value)}
                value={ends}
                placeholder="mm/dd/yyyy"
                required
                type="date"
                name="ends"
                id="ends"
                className="form-control"
              />
              <label htmlFor="ends">End Date</label>
            </div>
            <label htmlFor="description">Description</label>
            <div className="form-floating mb-3">
              <textarea
                onChange={(e) => setDescription(e.target.value)}
                value={description}
                required
                type="text"
                name="description"
                id="description"
                className="form-control"
                rows="2"
                style={{ height: '100%' }}
              ></textarea>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setMaxPresentations(e.target.value)}
                value={maxPresentations}
                placeholder="Maximum presentations"
                required
                type="number"
                name="max_presentations"
                id="max_presentations"
                className="form-control"
              />
              <label htmlFor="max_presentations">Maximum Presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => setMaxAttendees(e.target.value)}
                value={maxAttendees}
                placeholder="Maximum attendees"
                required
                type="number"
                name="max_attendees"
                id="max_attendees"
                className="form-control"
              />
              <label htmlFor="max_attendees ">Maximum Attendees</label>
            </div>
            <div className="mb-3">
              <select
                onChange={(e) => setLocation(e.target.value)}
                value={location}
                required
                id="location"
                name="location"
                className="form-select"
              >
                <option value="">Choose a location</option>
                {locations.map((location) => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button onClick={handleSubmit} className="btn btn-primary">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ConferenceForm;
